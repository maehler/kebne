#include <iostream>

#include "config.h"
#include "kebne.h"

int main(int argc, char** argv) {
    std::cout << "Hello world!" << std::endl;
    std::cout << "This is " << PACKAGE_VERSION << std::endl;
    return 0;
}
